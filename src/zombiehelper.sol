//SARAH N'GOTTA | IMAC 3
// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.13;

import "./zombiefeeding.sol";

contract ZombieHelper is ZombieFeeding {

  //Define a uint named levelUpFee, and set it equal to 0.001 ether.
  uint levelUpFee = 0.001 ether;

  // Start here
  // 1. Define levelUpFee

  modifier aboveLevel(uint _level, uint _zombieId) {
    require(zombies[_zombieId].level >= _level);
    _;
  }

//Create a withdraw function in our contract, which should be identical to the GetPaid example above.
  function withdraw() external onlyOwner {
    address payable _owner = payable(owner()); 
    _owner.transfer(address(this).balance);

  }

//Create a function named levelUp. It will take one parameter, _zombieId, a uint. It should be external and payable.

  function setLevelUpFee(uint _fee) external onlyOwner {
    levelUpFee = _fee;
  }

  function levelUp(uint _zombieId) external payable {
    require(msg.value == levelUpFee);
    zombies[_zombieId].level++;
  }

  // 2. Insert levelUp


    function changeName(uint _zombieId, string calldata _newName) external aboveLevel(2, _zombieId) onlyOwnerOf(_zombieId) {
    zombies[_zombieId].name = _newName;
  }

    function changeDna(uint _zombieId, uint _newDna) external aboveLevel(20, _zombieId) onlyOwnerOf(_zombieId) {
    zombies[_zombieId].dna = _newDna;
  }

  function getZombiesByOwner(address _owner) external view returns(uint[] memory) {
        uint[] memory result = new uint[](ownerZombieCount[_owner]);
        uint counter = 0;
    for (uint i = 0; i < zombies.length; i++) {
      if (zombieToOwner[i] == _owner) {
        result[counter] = i;
        counter++;
      }
    }
        return result;
  }
}