//SARAH N'GOTTA | IMAC 3
// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.13;
import "./zombiehelper.sol";

contract ZombieAttack is ZombieHelper {
uint randNonce = 0;
uint attackVictoryProbability = 70;

  //Create a function called randMod (random-modulus). It will be an internal function that takes a uint named _modulus, and returns a uint.
  function randMod(uint _modulus) internal returns(uint) {
    randNonce++;
    return uint(keccak256(abi.encodePacked(block.timestamp, msg.sender, randNonce))) % _modulus;
  }

  //Create a function called attack. It will take two parameters: _zombieId (a uint) and _targetId (also a uint). It should be an external function.
  function attack(uint _zombieId, uint _targetId) external onlyOwnerOf(_zombieId) {
    Zombie storage myZombie = zombies[_zombieId];
    Zombie storage enemyZombie = zombies[_targetId];
    uint rand = randMod(100);

    if (rand <= attackVictoryProbability) {
      myZombie.winCount++;
      myZombie.level++;
      enemyZombie.lossCount++;
      feedAndMultiply(_zombieId, enemyZombie.dna, "zombie");
    } else {
      myZombie.lossCount++;
      enemyZombie.winCount++;
      _triggerCooldown(myZombie);
    }
  }
}
