import "./style.css";

// Modules.
// import { createPublicClient, http, getContract, createWalletClient, custom } from "viem";
import { createPublicClient, http, getContract, createWalletClient, custom, parseEther } from "viem";
import { goerli } from "viem/chains";
import { CryptoZombies } from "./abi/CryptoZombies";

//------connexion wallet----------
// my EVM adress : 0xdAbE5Af29B080e9DF9bef3e5Ba61F286630Ebf8E 
// my token adress : 0x38A167E9b18cD114aFeBe7e8a7e5cD071C36896d
// my cryptozombie adress : 0x7cfF3E6De2Dbf01b1C6bE6dEA4addA37E3960597


const publicClient = createPublicClient({
  chain: goerli,
  transport: http(),
});

const [account] = await window.ethereum.request({ method: "eth_requestAccounts" });

const walletClient = createWalletClient({
  account,
  chain: goerli,
  transport: custom(window.ethereum),
});

const zombieContract = getContract({
  //adress UNI :
  address: "0x7cfF3E6De2Dbf01b1C6bE6dEA4addA37E3960597", 
  abi: CryptoZombies,
  publicClient,
  walletClient,
});

//cryptozombie ACTIONS
const blockNumber = await publicClient.getBlockNumber();
const ownerAddress = await zombieContract.read.owner();
const balanceOfOwner = await zombieContract.read.balanceOf([account]);
const zombiesByOwner = await zombieContract.read.getZombiesByOwner([account]);

const zombieInfos = [];
const zombieIds=[];


  for (let zombieId = 0; zombieId < zombieIds.length; zombieId++) {
    const info = await zombieContract.read.zombies([zombieId]);
    zombieInfos.push(info);
  }




//FONCTIONS
document.querySelector("#app").innerHTML = `
  <div>
  <h1>🧟‍♀️😱My Zombies Team😱🧟‍♀️</h1>
  <h2>Informations : </h2>
    <p>Current block is ${blockNumber}</p>
    <p>Owner's Adress : ${ownerAddress}</p>
    <p>Owner's Balance : ${balanceOfOwner}</p>
    <p>Owner's Zombie n° ${zombiesByOwner}</p>

  <h2>🧟‍♀️🏭 The Zombies Factory 🏭🧟‍♀️</h2>
  <div class="zombies-gallery">
    <div class="zombie-container">
     <h3>Zombie Creation</h3>
     <form>
        <label Name of your zombie:</label>
        <input type="text" id="zombieName" name="name">
        <button id="createNewZombie"> Create </button>
      </form>
      <span id="newZombieSpan"></span>
    </div>
    <div class="zombie-container">  
      <h3>Level up a zombie </h3>
      <button id="levelUpZombie"> Level up your zombie </button>
    </div>
  </div> 

  <h2>🧟‍♀️🖼️ The Zombies Gallery 🖼️🧟‍♀️</h2>
  <div class="zombies-gallery">
    ${zombieInfos.map((info, zombieId) => `
    <div class="zombie-container">
        <h3>Zombie ${zombieId+1}</h3>
        <ul>
          <li>Name: ${info[0]}</li>
          <li>DNA: ${info[1]}</li>
          <li>DNA: ${info[2]}</li>
          <li>Level: ${info[3]}</li>
          <li>Wins: ${info[4]}</li>
          <li>Losses: ${info[5]}</li>
          <li>Ready Time: ${info[6]}</li>
        </ul>
      </div>
    `).join('')}
  </div>

  <h2>🧟‍♀️✨ The New Zombies Gallery ✨🧟‍♀️</h2>
  <div class="gallery-container">
      <div class="zombie-container">
       
    </div>
  </div>
`;

document.querySelector("#createNewZombie").addEventListener("click", async(event) => {
  event.preventDefault();
  try {
  const hash = await zombieContract.write.createRandomZombie([document.querySelector("#zombieName").value]);
  document.querySelector("#newZombieSpan").innerHTML = `Waiting for new zombie`;
  const transaction = await publicClient.waitForTransactionReceipt({ hash: `${hash}` });
  if (transaction.status == "success") {
    document.querySelector("#newZombieSpan").innerHTML = `Transaction confirmed!`;

    // Ajouter l'ID du nouveau zombie à zombieIds
    const newZombieId = zombieIds.length;
      zombieIds.push(newZombieId);

       // Mettre à jour la galerie avec le nouveau zombie
       const newZombieInfo = await zombieContract.read.zombies([newZombieId]);
      zombieInfos.push(newZombieInfo);

      // Rafraîchir l'affichage de la galerie
      updateGallery();

  } else {
      document.querySelector("#newZombieSpan").innerHTML = `Transaction failed!`;
  };
  }catch (error) {
    console.error("Erreur lors de la création d'un nouveau zombie :", error);
  }
});

//Level up
document.querySelector("#levelUpZombie").addEventListener("click", async () => {
  try {
      if (balanceOfOwner === 0) {
          alert("You need to create a zombie before leveling up!");
          return;
      }

      const zombieIdToLevelUp = zombieIds[newZombieId];

      const levelUpTx = await walletClient.writeContract({
          address: zombieContract.address,
          abi: CryptoZombies,
          functionName: 'levelUp',
          args: [zombieIdToLevelUp],
          value: parseEther('0.001')
      });

      zombieInfos[zombieIdToLevelUp] = await zombieContract.read.zombies([zombieIdToLevelUp]);

  } catch (error) {
      console.error("Error while leveling up the zombie", error);
      alert("Error while leveling up the zombie");
  }


//update de la gallery
function updateGallery() {
  document.querySelector(".zombies-gallery").innerHTML = `
    ${zombieInfos.map((info, zombieId) => `
      <div class="zombie-container">
          <h3>Zombie ${zombieId + 1}</h3>
          <ul>
            <li>Name: ${info[0]}</li>
            <li>DNA: ${info[1]}</li>
            <li>DNA: ${info[2]}</li>
            <li>Level: ${info[3]}</li>
            <li>Wins: ${info[4]}</li>
            <li>Losses: ${info[5]}</li>
            <li>Ready Time: ${info[6]}</li>
          </ul>
        </div>
    `).join('')}
  `;
}
});

//probleme d'affichage de mes zombies ??
// impossible de créer plus d'1 zombie ??? "transaction failed" "you already have created a zombie #1012"???